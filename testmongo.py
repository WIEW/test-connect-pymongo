import pymongo

maxSevSelDelay = 1

try:
    client = pymongo.MongoClient("mongodb://doadmin:6R25TuvO8lZ703I1@realty-plenty-db-5159d3d8.mongo.ondigitalocean.com:27017/admin", serverSelectionTimeoutMS=maxSevSelDelay)
    print(client)
    client.server_info() # force connection on a request as the
                         # connect=True parameter of MongoClient seems
                         # to be useless here 
except pymongo.errors.ServerSelectionTimeoutError as err:
    # do whatever you need
    print(err)
